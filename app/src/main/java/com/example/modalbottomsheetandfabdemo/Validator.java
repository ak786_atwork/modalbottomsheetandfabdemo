package com.example.modalbottomsheetandfabdemo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static boolean isPasswordValid(String email) {
        String PASSWORD_PATTERN =  "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
        return Pattern.matches(PASSWORD_PATTERN,email);
    }

    public static boolean isEmailValid(String email) {
        String EMAIL_PATTERN = "[\\w+-]+(\\.[\\w+-]+)*@([\\w-]+\\.)+[\\w-]+";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        return pattern.matcher(email).matches();
    }
    public static boolean isUserNameValid(String username) {
        return username != null;
    }

    public static boolean isPhoneNumberValid(String phoneNumber){
        // The given argument to compile() method
        // is regular expression. With the help of
        // regular expression we can validate mobile
        // number.
        // 1) Begins with 0 or 91
        // 2) Then contains 7 or 8 or 9.
        // 3) Then contains 9 digits
        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");

        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        Matcher m = p.matcher(phoneNumber);
        return (m.find() && m.group().equals(phoneNumber));
    }

}
