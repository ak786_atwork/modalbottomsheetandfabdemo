package com.example.modalbottomsheetandfabdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ValidationActivity extends AppCompatActivity {

    EditText email, password,phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        phone = findViewById(R.id.phone);
    }

    public void validateEmail(View view) {
        String e = email.getText().toString();
//        Validator.isEmailValid(e);
        showToast(Validator.isEmailValid(e)+"");
    }

    private void showToast(String info) {
        Toast.makeText(this,info,Toast.LENGTH_SHORT).show();
    }

    public void validatePassword(View view) {
        String e = password.getText().toString();
//        Validator.isEmailValid(e);
        showToast(Validator.isPasswordValid(e)+"");
    }

    public void validatePhone(View view) {
        String e = phone.getText().toString();
//        Validator.isEmailValid(e);
        showToast(Validator.isPhoneNumberValid(e)+"");
    }
}
